#!/bin/bash

IMAGE_NAME=$DOCKER_IMAGE:latest

if docker ps -a | grep $CONTAINER_NAME ; then
  echo "Deleting existing $CONTAINER_NAME container"
  docker container stop $CONTAINER_NAME
  docker container rm -f $CONTAINER_NAME

  echo "Deleting existing $DOCKER_IMAGE image"
  docker rmi $DOCKER_IMAGE
fi

echo "Pulling new version of $DOCKER_IMAGE image"
docker pull $IMAGE_NAME

echo "Creating new version of $CONTAINER_NAME container"
docker run -p 8080:8080 --name $CONTAINER_NAME -td $IMAGE_NAME