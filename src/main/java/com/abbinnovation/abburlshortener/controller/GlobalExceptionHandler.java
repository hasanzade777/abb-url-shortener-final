package com.abbinnovation.abburlshortener.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    public void notFound(HttpServletResponse rs, HttpSession session, Exception ex) throws IOException {
        log.error("Current SessionId: {}. Exception message: {}", session.getId(), ex.getMessage());
        if (ex instanceof AuthenticationException){
            rs.sendRedirect("/errorPage/403");
            return;
        }
        rs.sendRedirect("/errorPage/404");
    }
}
