package com.abbinnovation.abburlshortener.securityV3.service;

import com.abbinnovation.abburlshortener.model.User;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;

public interface AuthenticationService {
    Authentication authenticate(String jwt, HttpServletRequest req);

    Authentication authenticate(String username, String password);

    String delegateTokenCreation(User user);

    boolean delegateTokenValidation(String jwt);
}
