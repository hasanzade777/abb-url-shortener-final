package com.abbinnovation.abburlshortener.securityV3.filter;

import com.abbinnovation.abburlshortener.model.User;
import com.abbinnovation.abburlshortener.securityV3.service.AuthenticationService;
import com.abbinnovation.abburlshortener.utils.MutableHttpServletRequest;
import com.abbinnovation.abburlshortener.utils.StaticHashMap;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RequiredArgsConstructor
@Slf4j
public class CustomAuthorizationFilter extends OncePerRequestFilter {
    private static final String WHITE_LIST_URI = "(/login|/signup|/favicon.ico)";
    private final AuthenticationService authenticationService;
    private final HandlerExceptionResolver resolver;

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request,
                                    @NonNull HttpServletResponse response,
                                    @NonNull FilterChain filterChain) throws ServletException, IOException {
        MutableHttpServletRequest mutableRequest = new MutableHttpServletRequest(request);

        if (!request.getServletPath().matches(WHITE_LIST_URI) || !request.getMethod().equals(HttpMethod.GET.name())) {
            try {
                String username = request.getParameter("email");
                String password = request.getParameter("password");
                Authentication authentication = authenticationService.authenticate(username, password);

                if (authentication.isAuthenticated()) {
                    User user = (User) authentication.getPrincipal();
                    String jwt = authenticationService.delegateTokenCreation(user);

                    StaticHashMap.put(user.getEmail(), jwt);
                    mutableRequest.putHeader("Authorization", "Bearer " + jwt);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            } catch (Exception ex) {
                resolver.resolveException(request, response, null, ex);
                return;
            }
        }
        filterChain.doFilter(mutableRequest, response);
    }
}
