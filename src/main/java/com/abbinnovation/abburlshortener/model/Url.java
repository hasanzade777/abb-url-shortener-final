package com.abbinnovation.abburlshortener.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
@Entity
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "URL", schema = "public")
public class Url {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "CREATION_DATE")
    LocalDate creationDate;

    @Column(name = "FULL_URL")
    String fullUrl;

    @Column(name = "SHORT_URL")
    String shortUrl;

    @Column(name = "VISIT_COUNT")
    Integer visitCount;

    @Column(name = "USER_ID")
    Long userId;

    @Column(name = "URL_ID")
    @OneToMany(mappedBy = "urlId", cascade = CascadeType.ALL)
    @JsonIgnore
    Set<UrlVisitCountLog> urlVisitCountLogs = new HashSet<>();

}