package com.abbinnovation.abburlshortener.service.impl;

import com.abbinnovation.abburlshortener.dto.req.UserForm;
import com.abbinnovation.abburlshortener.mapper.UserMapper;
import com.abbinnovation.abburlshortener.model.User;
import com.abbinnovation.abburlshortener.repository.UserRepository;
import com.abbinnovation.abburlshortener.service.EmailService;
import com.abbinnovation.abburlshortener.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.utility.RandomString;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserMapper userMapper;
    private final UserRepository userRepo;
    private final EmailService emailService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public User createUser(UserForm userForm) {
        User user = dtoToEntity(userForm);
        return userRepo.save(user);
    }

    @Override
    public boolean userExistsByEmail(String email) {
        return userRepo.existsByEmail(email);
    }

    @Override
    public String getUserName(User user) {
        return user.getName().concat(" ").concat(user.getSurname());
    }

    private User dtoToEntity(UserForm userForm) {
        return userMapper.userFormToUser(userForm);
    }

    @Override
    public void forgotPassword(String email) {
        if (userRepo.existsByEmail(email)) {
            String newPassword = RandomString.make(30);
            emailService.sendEmail(email, newPassword);
            resetPassword(email, newPassword);
        } else {
            log.error("User with indicated email = {} doesn't exist", email);
        }
    }

    private void resetPassword(String email, String newPassword) {
        var user = userRepo.findByEmail(email).get();
        String encodedPassword = passwordEncoder.encode(newPassword);
        user.setPassword(encodedPassword);
        userRepo.save(user);
    }
}
