package com.abbinnovation.abburlshortener.controller;

import com.abbinnovation.abburlshortener.dto.url.UrlVisitCountLogDto;
import com.abbinnovation.abburlshortener.model.User;
import com.abbinnovation.abburlshortener.service.UrlShortenerService;
import com.abbinnovation.abburlshortener.service.UrlVisitCountLogService;
import com.abbinnovation.abburlshortener.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Controller
@RequestMapping("/url")
@Slf4j
public class UrlController {
    private final UrlShortenerService urlShortenerService;
    private final UrlVisitCountLogService visitCountService;
    private final UserService userService;


    @GetMapping("/landing")
    public String getLandingPage(@RequestHeader("Authorization") final String header,
                                 @AuthenticationPrincipal User user, Model model) {
        model.addAttribute("userName", userService.getUserName(user));
        return "landing";
    }

    @GetMapping("/mainPage")
    public String getMainPage(@RequestHeader("Authorization") final String header,
                              @AuthenticationPrincipal User user,
                              Model model) {
        model.addAttribute("mainPageData", urlShortenerService.getTheMainPage(user.getId()));
        model.addAttribute("userName", userService.getUserName(user));
        return "main-page";
    }

    @PostMapping("/shortenUrl")
    public ResponseEntity<String> shortTheUrl(@RequestHeader("Authorization") final String header,
                                              @AuthenticationPrincipal User user, @RequestBody String fullUrl) {
        return ResponseEntity.ok(urlShortenerService.shortenUrl(user.getId(), fullUrl));
    }

    @ResponseBody
    @GetMapping("/visitCountLogs/{shortUrl}")
    ResponseEntity<List<UrlVisitCountLogDto>> getVisitCountLogsBuShortUrl(@PathVariable("shortUrl") String shortUrl) {
        Optional<List<UrlVisitCountLogDto>> logs = visitCountService.getVisitCountLogsByShortUrl(shortUrl);
        return logs.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }

}
