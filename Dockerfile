FROM gradle:jdk19-alpine as gradle

WORKDIR /usr/src/app
COPY . /usr/src/app

RUN gradle clean build

FROM eclipse-temurin:19.0.2_7-jdk-jammy
ENV JAVA_HOME=/opt/java/openjdk

ARG JAR_FILE=Abb-Url-Shortener-0.0.1-SNAPSHOT.jar

WORKDIR /opt/app

COPY --from=gradle /usr/src/app/build/libs/${JAR_FILE} /opt/app/myapp.jar

ENTRYPOINT ["java", "-jar", "myapp.jar"]
