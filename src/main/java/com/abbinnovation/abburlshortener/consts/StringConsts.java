package com.abbinnovation.abburlshortener.consts;

public class StringConsts {

    private StringConsts() {
    }

    public static String SHORT_URL_DOMAIN = "https://shorten-url.me/";
    public static String HTTPS = "https://";

}
