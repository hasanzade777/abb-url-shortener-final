package com.abbinnovation.abburlshortener.securityV3.service.impl;

import com.abbinnovation.abburlshortener.model.User;
import com.abbinnovation.abburlshortener.securityV3.service.AuthenticationService;
import com.abbinnovation.abburlshortener.securityV3.service.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    private final UserDetailsService userDetailsService;
    private final JwtService jwtService;
    private final AuthenticationProvider authenticationManager;

    @Override
    public Authentication authenticate(String jwt, HttpServletRequest req) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(jwtService.extractUser(jwt));
        UsernamePasswordAuthenticationToken authToken =
                new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));

        return authToken;
    }

    @Override
    public Authentication authenticate(String username, String password) {
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username, password);

        return authenticationManager.authenticate(authToken);
    }

    @Override
    public String delegateTokenCreation(User user) {
        return jwtService.generateToken(user);
    }

    @Override
    public boolean delegateTokenValidation(String jwt) {
        try {
            return jwtService
                    .isTokenValid(jwt,
                            userDetailsService.loadUserByUsername(jwtService.extractUser(jwt)));
        } catch (Exception e) {
            throw new BadCredentialsException("Bad credentials. Please login again");
        }
    }
}
