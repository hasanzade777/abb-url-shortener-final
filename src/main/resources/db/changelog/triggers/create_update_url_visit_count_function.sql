CREATE OR REPLACE FUNCTION update_url_visit_count()
    RETURNS TRIGGER AS
$$
BEGIN
    UPDATE url
    SET visit_count = visit_count + (NEW.visit_count - OLD.visit_count)
    WHERE id = NEW.url_id;
    RETURN NEW;
END;
$$
    LANGUAGE plpgsql;