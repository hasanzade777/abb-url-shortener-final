package com.abbinnovation.abburlshortener.securityV3.filter;

import com.abbinnovation.abburlshortener.model.User;
import com.abbinnovation.abburlshortener.utils.MutableHttpServletRequest;
import com.abbinnovation.abburlshortener.utils.StaticHashMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RequiredArgsConstructor
@Slf4j
public class CustomAuthenticationFilter implements Filter {
    private static final String CONTEXT_ATTRIBUTE = "SPRING_SECURITY_CONTEXT";

    private final HandlerExceptionResolver resolver;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        MutableHttpServletRequest mutableRequest = new MutableHttpServletRequest((HttpServletRequest) request);
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        if (req.getRequestURI().matches("/url/.*")) {
            try {
                Optional<SecurityContext> context =
                        Optional.ofNullable((SecurityContext) req.getSession().getAttribute(CONTEXT_ATTRIBUTE));

                if (context.isEmpty() || !context.get().getAuthentication().isAuthenticated()) {
                    throw new BadCredentialsException("Session is expired. Please login again");
                }
                User principal = (User) context.get().getAuthentication().getPrincipal();
                String token = StaticHashMap.get(principal.getEmail());
                mutableRequest.putHeader(AUTHORIZATION, "Bearer " + token);
            } catch (Exception ex) {
                resolver.resolveException(req, res, null, ex);
                return;
            }
        }
        chain.doFilter(mutableRequest, response);
    }
}