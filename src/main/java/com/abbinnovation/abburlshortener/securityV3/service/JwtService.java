package com.abbinnovation.abburlshortener.securityV3.service;

import io.jsonwebtoken.Claims;
import java.util.Map;
import java.util.function.Function;
import org.springframework.security.core.userdetails.UserDetails;

public interface JwtService {
    String extractUser(String jwt);

    <T> T extractClaim(String token, Function<Claims, T> claimsResolver);

    String generateToken(UserDetails userDetails);

    String generateToken(Map<String, Object> claimsMap, UserDetails userDetails);

    boolean isTokenValid(String token, UserDetails userDetails);
}
