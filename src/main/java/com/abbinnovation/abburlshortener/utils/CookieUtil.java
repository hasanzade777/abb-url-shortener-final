package com.abbinnovation.abburlshortener.utils;

import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class CookieUtil {
    public static void extendSessionExpiration(HttpServletRequest req, HttpServletResponse resp,
                                               Integer rememberMeTimeSeconds) {
        Optional.ofNullable(WebUtils.getCookie(req, "SESSION"))
                .ifPresent(cookie -> {
                    cookie.setMaxAge(rememberMeTimeSeconds);
                    req.getSession().setMaxInactiveInterval(rememberMeTimeSeconds);
                    resp.addCookie(cookie);
                });
    }
}
