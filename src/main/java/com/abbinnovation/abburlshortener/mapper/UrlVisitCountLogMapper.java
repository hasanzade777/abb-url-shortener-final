package com.abbinnovation.abburlshortener.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.abbinnovation.abburlshortener.dto.url.UrlVisitCountLogDto;
import com.abbinnovation.abburlshortener.model.UrlVisitCountLog;
import com.abbinnovation.abburlshortener.utils.Formatter;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(unmappedTargetPolicy = IGNORE, componentModel = "spring")
public interface UrlVisitCountLogMapper {
    @Mapping(source = "urlId.shortUrl", target = "shortUrl", qualifiedByName = "formatShortUrl")
    UrlVisitCountLogDto entityToUrlVisitCountLogDto(UrlVisitCountLog urlVisitCountLog);

    List<UrlVisitCountLogDto> entityToUrlVisitCountLogDtoList(List<UrlVisitCountLog> urlVisitCountLogs);

    @Named("formatShortUrl")
    default String formatShortUrl(String shortUrl) {
        return Formatter.formatShortUrl(shortUrl);
    }

}
