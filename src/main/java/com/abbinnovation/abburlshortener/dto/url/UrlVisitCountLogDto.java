package com.abbinnovation.abburlshortener.dto.url;

import com.abbinnovation.abburlshortener.utils.Formatter;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UrlVisitCountLogDto {
    @JsonFormat(pattern = Formatter.dateFormatSchema)
    LocalDate visitDate;
    String shortUrl;
    Integer visitCount;
}
