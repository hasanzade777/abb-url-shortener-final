package com.abbinnovation.abburlshortener.utils;

import java.util.HashMap;
import java.util.Map;

public class StaticHashMap {

    private static final Map<String, String> TOKEN_STORAGE = new HashMap<>();

    public static void put(String key, String value) {
        TOKEN_STORAGE.put(key, value);
    }

    public static String get(String key) {
        return TOKEN_STORAGE.get(key);
    }

    public static boolean containsKey(String key) {
        return TOKEN_STORAGE.containsKey(key);
    }

    public static boolean isEmpty() {
        return TOKEN_STORAGE.isEmpty();
    }
}
