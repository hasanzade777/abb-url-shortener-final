package com.abbinnovation.abburlshortener.controller;

import com.abbinnovation.abburlshortener.dto.req.UserForm;
import com.abbinnovation.abburlshortener.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RequiredArgsConstructor
@Controller
@RequestMapping("/signup")
public class RegistrationController {
    private final UserService userService;

    @GetMapping
    public String showForm(Model model) {
        model.addAttribute("user", new UserForm());
        return "registration";
    }

    @PostMapping
    public String register(@ModelAttribute(name = "user") @Valid UserForm userForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        if (userService.userExistsByEmail(userForm.getEmail())) {
            bindingResult.rejectValue("email", "error.user", "An account already exists by this email");
            return "registration";
        }
        userService.createUser(userForm);
        return "redirect:/login";
    }

}
