package com.abbinnovation.abburlshortener.service.impl;

import com.abbinnovation.abburlshortener.dto.url.UrlVisitCountLogDto;
import com.abbinnovation.abburlshortener.mapper.UrlVisitCountLogMapper;
import com.abbinnovation.abburlshortener.model.Url;
import com.abbinnovation.abburlshortener.model.UrlVisitCountLog;
import com.abbinnovation.abburlshortener.repository.UrlRepository;
import com.abbinnovation.abburlshortener.service.UrlVisitCountLogService;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@RequiredArgsConstructor
@Service
public class UrlVisitCountLogServiceImpl implements UrlVisitCountLogService {

    private final UrlRepository urlRepo;
    private final UrlVisitCountLogMapper urlVisitCountLogMapper;

    @Override
    public Optional<List<UrlVisitCountLogDto>> getVisitCountLogsByShortUrl(String shortUrl) {
        return urlRepo.findByShortUrl(shortUrl)
                .map(urlEntity -> urlVisitCountLogMapper.entityToUrlVisitCountLogDtoList(
                        urlEntity.getUrlVisitCountLogs()
                                .stream().sorted(Comparator.comparing(UrlVisitCountLog::getVisitDate)).toList()));
    }

    @Override
    public Optional<Url> getUrlByShortUrl(String shortUrl) {
        return urlRepo.findByShortUrl(shortUrl);
    }

    @Override
    public void updateVisitCount(Url url) {
        urlRepo.updateVisitCount(url.getId());
    }
}
