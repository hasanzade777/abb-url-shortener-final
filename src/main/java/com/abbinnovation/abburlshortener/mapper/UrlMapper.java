package com.abbinnovation.abburlshortener.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.abbinnovation.abburlshortener.dto.url.UrlDto;
import com.abbinnovation.abburlshortener.model.Url;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(unmappedTargetPolicy = IGNORE, componentModel = "spring")
public interface UrlMapper {
    List<UrlDto> entityToUrlDto(List<Url> url);
}
