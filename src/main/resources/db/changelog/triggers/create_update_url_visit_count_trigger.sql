CREATE TRIGGER update_url_entity_visit_count_trigger
    AFTER INSERT OR UPDATE OF visit_count
    ON url_visit_count_log
    FOR EACH ROW
EXECUTE FUNCTION update_url_visit_count();