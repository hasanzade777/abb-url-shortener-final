package com.abbinnovation.abburlshortener.service;

import com.abbinnovation.abburlshortener.dto.url.UrlDto;
import java.util.List;

public interface UrlShortenerService {
    String shortenUrl(Long userId, String fullUrl);

    List<UrlDto> getTheMainPage(Long userId);
}
