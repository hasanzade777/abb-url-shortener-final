function validateForm() {
    const email = document.getElementById("email");
    const emailValue = email.value.trim();
    const emailRegex = /^[^@\s]+@[^@\s]+\.[^@\s]+$/;
    const emailError = document.getElementById("emailError");

    const pwd = document.getElementById("password");
    const pwdValue = pwd.value.trim();
    const pwdRegex = /^.+$/;
    const pwdError = document.getElementById("passwordError");
    let isValid = true;

    if (!emailRegex.test(emailValue)) {
        emailError.textContent = "Please enter a valid email address";
        isValid = false;
    }

    if (!pwdRegex.test(pwdValue)) {
        pwdError.textContent = "Please enter a valid password";
        isValid = false;
    }
    const myError = document.getElementById("myError");
    if (myError != null) myError.textContent = "";

    if (!isValid) return;

    emailError.textContent = "";
    pwdError.textContent = "";
    $("#myForm").submit();
}