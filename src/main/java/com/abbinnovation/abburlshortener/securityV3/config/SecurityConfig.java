package com.abbinnovation.abburlshortener.securityV3.config;

import com.abbinnovation.abburlshortener.securityV3.filter.CustomAuthenticationFilter;
import com.abbinnovation.abburlshortener.securityV3.filter.CustomAuthorizationFilter;
import com.abbinnovation.abburlshortener.securityV3.filter.HeaderValidationFilter;
import com.abbinnovation.abburlshortener.securityV3.service.AuthenticationService;
import com.abbinnovation.abburlshortener.securityV3.service.JwtService;
import com.abbinnovation.abburlshortener.securityV3.service.impl.AuthenticationServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {
    private final UserDetailsService detailsService;
    private final JwtService jwtService;

    @Bean
    protected SecurityFilterChain configure(HttpSecurity http) throws Exception {
        http
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                    .maximumSessions(1)
                    .expiredUrl("/login")
                .and()
                    .sessionFixation().migrateSession()
                .and()
                    .csrf().disable()
                .authorizeRequests()
                    .antMatchers("/login", "/signup", "/forgot-password", "/{shortUrl}", "/").permitAll()
                .and()
                .authorizeRequests()
                    .antMatchers("/url/**").authenticated()
                .and()
                    .authorizeRequests().anyRequest().authenticated()
                .and()
                    .formLogin().disable()
                .logout().logoutUrl("/logout")
                .deleteCookies()
                .logoutSuccessUrl("/login")
                .and()
                .addFilterAfter(new CustomAuthenticationFilter(resolver()), UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(new HeaderValidationFilter(authenticationService(), resolver()), UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }

    @Bean
    @Order(1)
    public FilterRegistrationBean<CustomAuthorizationFilter> jwtFilter() {
        FilterRegistrationBean<CustomAuthorizationFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new CustomAuthorizationFilter(authenticationService(), resolver()));
        registrationBean.addUrlPatterns("/login");
        return registrationBean;
    }

    @Bean
    public ExceptionHandlerExceptionResolver resolver() {
     return new ExceptionHandlerExceptionResolver();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationProvider customAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();

        provider.setPasswordEncoder(passwordEncoder());
        provider.setUserDetailsService(detailsService);

        return provider;
    }

    @Bean
    public AuthenticationService authenticationService() {
        return new AuthenticationServiceImpl(detailsService, jwtService, customAuthenticationProvider());
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring().antMatchers("/static/**", "/favicon.ico", "/errorPage/{code}");
    }

    @Bean
    public ServletContextInitializer servletContextInitializer() {
        return servletContext -> servletContext.getSessionCookieConfig().setName("SESSION");
    }
}
