package com.abbinnovation.abburlshortener.repository;

import com.abbinnovation.abburlshortener.model.Url;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UrlRepository extends JpaRepository<Url, Long> {

    Optional<Url> findByShortUrl(String shortUrl);

    Optional<List<Url>> findByUserId(Long userId);

    Optional<Url> findByFullUrlAndUserId(String fullUrl, Long userId);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value =
                    "INSERT INTO url_visit_count_log (url_id, visit_date, visit_count)\n" +
                            "VALUES (?1, CURRENT_DATE, 1)\n" +
                            "ON CONFLICT (url_id, visit_date)\n" +
                            "DO UPDATE SET visit_count = url_visit_count_log.visit_count + 1;" +
                            "UPDATE url\n" +
                            "SET visit_count = url.visit_count + 1\n" +
                            "WHERE id = ?1")
    void updateVisitCount(Long urlId);
}
