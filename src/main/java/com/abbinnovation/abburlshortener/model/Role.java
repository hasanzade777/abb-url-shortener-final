package com.abbinnovation.abburlshortener.model;

public enum Role {
    ADMIN, USER
}
