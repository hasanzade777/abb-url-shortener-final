package com.abbinnovation.abburlshortener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrlShortenerStartApp {

    public static void main(String[] args) {
        SpringApplication.run(UrlShortenerStartApp.class, args);
    }

}
