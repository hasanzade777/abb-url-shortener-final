package com.abbinnovation.abburlshortener.utils;

import com.abbinnovation.abburlshortener.consts.StringConsts;

public class Formatter {

    private Formatter() {
    }

    public static final String dateFormatSchema = "dd/MM/yyyy";

    public static String formatShortUrl(String shortUrl) {
        return StringConsts.SHORT_URL_DOMAIN + shortUrl;
    }

}
