package com.abbinnovation.abburlshortener.dto.req;

import com.abbinnovation.abburlshortener.model.User;
import java.io.Serializable;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * A DTO for the {@link User} entity
 */
@Getter
@Setter
@ToString
public class UserForm implements Serializable {
    private static final String REQUIRED_FIELD_MSG = "This is a required field.";
    private static final String STRONG_PASSWORD_MSG =
            "Password must contain at least 8 characters with a combination of letters, numbers, and special " +
                    "characters.";
    private static final String EMAIL_VALIDATION_MSG = "Email should be in a valid format";

    public static final String PASSWORD_RGX = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$";

    @NotBlank(message = REQUIRED_FIELD_MSG)
    private String name;
    @NotBlank(message = REQUIRED_FIELD_MSG)
    private String surname;
    @NotBlank(message = REQUIRED_FIELD_MSG)
    @Email(message = EMAIL_VALIDATION_MSG)
    private String email;
    @NotBlank(message = REQUIRED_FIELD_MSG)
    @Pattern(regexp = PASSWORD_RGX, message = STRONG_PASSWORD_MSG)
    private String password;
}
