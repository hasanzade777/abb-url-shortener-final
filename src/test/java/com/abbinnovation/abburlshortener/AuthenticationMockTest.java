package com.abbinnovation.abburlshortener;

import com.abbinnovation.abburlshortener.model.User;
import com.abbinnovation.abburlshortener.repository.UserRepository;
import com.abbinnovation.abburlshortener.securityV3.service.JwtService;
import com.abbinnovation.abburlshortener.utils.StaticHashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticationMockTest {
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtService jwtService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    @Test
    void landingAuthenticated() throws Exception {
        MockHttpSession session = getMockHttpSession();

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/url/landing")
                        .session(session))
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/url/mainPage")
                        .session(session))
                .andExpect(status().isOk());
    }

    @Test
    void landingUnauthenticated() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/url/landing"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/errorPage/403")); // as filter throw BadCredentialException

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/url/mainPage"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/errorPage/403"));
    }

    private MockHttpSession getMockHttpSession() {
        User user = userRepository.findByEmail("jalal.aliy3v@gmail.com").orElseThrow();
        StaticHashMap.put(user.getEmail(), jwtService.generateToken(user));

        Authentication auth = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        securityContext.setAuthentication(auth);

        MockHttpSession session = new MockHttpSession();
        session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
        return session;
    }
}
