package com.abbinnovation.abburlshortener.service;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.abbinnovation.abburlshortener.dto.url.UrlVisitCountLogDto;
import com.abbinnovation.abburlshortener.mapper.UrlVisitCountLogMapper;
import com.abbinnovation.abburlshortener.model.Url;
import com.abbinnovation.abburlshortener.model.UrlVisitCountLog;
import com.abbinnovation.abburlshortener.repository.UrlRepository;
import com.abbinnovation.abburlshortener.service.impl.UrlVisitCountLogServiceImpl;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UrlVisitCountLogServiceImplTest {
    @Mock
    private UrlRepository urlRepository;
    @Mock
    private UrlVisitCountLogMapper urlVisitCountLogMapper;
    @InjectMocks
    private UrlVisitCountLogServiceImpl urlVisitCountLogService;

    private UrlVisitCountLogDto urlVisitCountLogDto;
    private UrlVisitCountLog urlVisitCountLog;
    private Url url;

    @BeforeEach
    void setup() {
        urlVisitCountLog = UrlVisitCountLog.builder()
                .id(1L)
                .urlId(url)
                .visitDate(LocalDate.of(2023, 1, 1))
                .visitCount(5)
                .build();
        url = Url.builder()
                .fullUrl("facebook.com")
                .shortUrl("2kDlMvbsQ")
                .creationDate(LocalDate.of(2023, 1, 1))
                .visitCount(100)
                .userId(1L)
                .urlVisitCountLogs(Set.of(urlVisitCountLog))
                .build();
    }

    @Test
    void getVisitCountLogsByShortUrlTest() {
        //Arrange
        when(urlRepository.findByShortUrl("2kDlMvbsQ")).thenReturn(Optional.of(url));
        when(urlVisitCountLogMapper.entityToUrlVisitCountLogDtoList(singletonList(urlVisitCountLog))).thenReturn(
                singletonList(urlVisitCountLogDto));
        //Act
        var urlVisitCountLogDtos = urlVisitCountLogService.getVisitCountLogsByShortUrl("2kDlMvbsQ");

        //Assert
        assertThat(urlVisitCountLogDtos).isNotNull();
        assertThat(urlVisitCountLogDtos).isPresent();
        assertThat(urlVisitCountLogDtos).isEqualTo(Optional.of(singletonList(urlVisitCountLogDto)));

    }

    @Test
    void getUrlByShortUrlTest() {
        //Arrange
        when(urlRepository.findByShortUrl("2kDlMvbsQ")).thenReturn(Optional.of(url));

        //Act
        var urlByShortUrl = urlVisitCountLogService.getUrlByShortUrl("2kDlMvbsQ");

        //Assert
        assertThat(urlByShortUrl).isNotNull();
        assertThat(urlByShortUrl).isNotEmpty();
        assertThat(urlByShortUrl).isEqualTo(Optional.of(url));
    }

    @Test
    void updateVisitCountTest() {
        //Arrange
        doNothing().when(urlRepository).updateVisitCount(url.getId());
        //Act
        urlVisitCountLogService.updateVisitCount(url);
        //Assert
        verify(urlRepository).updateVisitCount(url.getId());
    }
}
