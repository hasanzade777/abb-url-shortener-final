package com.abbinnovation.abburlshortener.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

@Controller
public class IntermediateController {

    /**
     * Just because we don't need this logo
     */
    @GetMapping("favicon.ico")
    @ResponseBody
    public void returnNoFavicon() {
    }

    @GetMapping("/errorPage/{code}")
    public String errorRedirect(@PathVariable("code") int code, HttpSession session, RedirectAttributes attributes) {
        if (code == 404) return "redirect:/error";

        session.invalidate();
        attributes.addFlashAttribute("error", "Please login again");
        return "redirect:/login";
    }
}
