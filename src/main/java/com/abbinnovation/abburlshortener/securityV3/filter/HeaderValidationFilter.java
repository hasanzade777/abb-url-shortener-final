package com.abbinnovation.abburlshortener.securityV3.filter;

import com.abbinnovation.abburlshortener.securityV3.service.AuthenticationService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RequiredArgsConstructor
@Slf4j
public class HeaderValidationFilter extends OncePerRequestFilter {

    private static final String SECURED_URI = "(/url/.*)";
    private static final String CONTEXT_ATTRIBUTE = "SPRING_SECURITY_CONTEXT";

    private final AuthenticationService authenticationService;
    private final HandlerExceptionResolver resolver;

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request,
                                    @NonNull HttpServletResponse response,
                                    @NonNull FilterChain filterChain) throws ServletException, IOException {
        Optional<SecurityContext> contextOpt =
                Optional.ofNullable((SecurityContext) request.getSession().getAttribute(CONTEXT_ATTRIBUTE));

        if (request.getRequestURI().matches("/login|/signup|/forgot-password") && contextOpt.isPresent()) {
            response.sendRedirect("/url/landing");
            return;
        } else if (request.getRequestURI().matches(SECURED_URI)) {
            try {
                Optional<String> headerOpt = Optional.ofNullable(request.getHeader(AUTHORIZATION)); // can be null
                if (headerOpt.isPresent() && headerOpt.get().startsWith("Bearer ")) {
                    String token = headerOpt.get().substring(7);
                    boolean isValid = authenticationService.delegateTokenValidation(token);
                    if (!isValid) throw new BadCredentialsException("Session is expired. Please login again");
                }
            } catch (Exception ex) {
                resolver.resolveException(request, response, null, ex);
                return;
            }
        }
        filterChain.doFilter(request, response);
    }
}