package com.abbinnovation.abburlshortener.service;

import com.abbinnovation.abburlshortener.dto.url.UrlVisitCountLogDto;
import com.abbinnovation.abburlshortener.model.Url;
import java.util.List;
import java.util.Optional;

public interface UrlVisitCountLogService {

    Optional<List<UrlVisitCountLogDto>> getVisitCountLogsByShortUrl(String shortUrl);

    Optional<Url> getUrlByShortUrl(String shortUrl);

    void updateVisitCount(Url url);
}
