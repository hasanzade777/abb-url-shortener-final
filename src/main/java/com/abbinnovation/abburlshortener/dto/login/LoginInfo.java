package com.abbinnovation.abburlshortener.dto.login;

import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginInfo {

    @NotEmpty(message = "User's email cannot be empty.")
    private String email;
    @NotEmpty(message = "User's password cannot be empty.")
    private String password;
    private Boolean isRemembered = false;
}
