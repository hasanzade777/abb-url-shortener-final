package com.abbinnovation.abburlshortener.mapper;

import com.abbinnovation.abburlshortener.dto.req.UserForm;
import com.abbinnovation.abburlshortener.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(unmappedTargetPolicy = IGNORE, componentModel = "spring")
public interface UserMapper {
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Mapping(target = "password", expression = "java(passwordEncoder.encode(userForm.getPassword()))")
    User userFormToUser(UserForm userForm);
}
