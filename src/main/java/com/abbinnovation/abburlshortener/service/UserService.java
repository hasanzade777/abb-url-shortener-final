package com.abbinnovation.abburlshortener.service;

import com.abbinnovation.abburlshortener.dto.req.UserForm;
import com.abbinnovation.abburlshortener.model.User;

public interface UserService {
    User createUser(UserForm userForm);

    boolean userExistsByEmail(String email);

    String getUserName(User user);

    void forgotPassword(String email);

}
