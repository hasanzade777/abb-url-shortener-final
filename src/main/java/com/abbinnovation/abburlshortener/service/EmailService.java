package com.abbinnovation.abburlshortener.service;

public interface EmailService {

    void sendEmail(String email, String newPassword);
}
