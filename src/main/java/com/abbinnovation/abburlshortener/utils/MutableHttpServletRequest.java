package com.abbinnovation.abburlshortener.utils;

import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

public class MutableHttpServletRequest extends ContentCachingRequestWrapper {

    private final Map<String, String> headers = new HashMap<>();

    public MutableHttpServletRequest(HttpServletRequest request) {
        super(request);
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headerValue = request.getHeader(headerName);
            headers.put(headerName, headerValue);
        }
    }

    @Override
    public String getHeader(String name) {
        Optional<String> header = Optional.ofNullable(headers.get(name));
        return header.orElseGet(() -> super.getHeader(name));
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        Set<String> headerNames = new HashSet<>(headers.keySet());
        Enumeration<String> superHeaderNames = super.getHeaderNames();
        while (superHeaderNames.hasMoreElements()) {
            headerNames.add(superHeaderNames.nextElement());
        }
        return Collections.enumeration(headerNames);
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
        Optional<String> header = Optional.ofNullable(headers.get(name));
        return header
                .map(value -> Collections.enumeration(Collections.singleton(value)))
                .orElseGet(() -> super.getHeaders(name));
    }

    public void putHeader(String name, String value) {
        headers.put(name, value);
    }

}
