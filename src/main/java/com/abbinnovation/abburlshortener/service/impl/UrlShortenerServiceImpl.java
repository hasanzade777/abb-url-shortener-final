package com.abbinnovation.abburlshortener.service.impl;

import com.abbinnovation.abburlshortener.dto.url.UrlDto;
import com.abbinnovation.abburlshortener.mapper.UrlMapper;
import com.abbinnovation.abburlshortener.model.Url;
import com.abbinnovation.abburlshortener.repository.UrlRepository;
import com.abbinnovation.abburlshortener.service.UrlShortenerService;
import com.abbinnovation.abburlshortener.utils.Formatter;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UrlShortenerServiceImpl implements UrlShortenerService {
    private final UrlRepository urlRepository;
    private final UrlMapper urlMapper;


    @Override
    public String shortenUrl(Long userId, String fullUrl) {
        fullUrl = fullUrl.replace("\"", "");
        if (fullUrl.isEmpty() || fullUrl.isBlank()) {
            return "URL can't be empty or blank this is message for ATTENTION";
        }
        var shortUrlExist = urlRepository.findByFullUrlAndUserId(fullUrl, userId);
        if (shortUrlExist.isEmpty()) {
            var shortUrl = randomShortUrlAndCheckDuplicate();
            saveUrl(userId, fullUrl, shortUrl);
            return getFormattedShortUrl(shortUrl);
        }
        return getFormattedShortUrl(shortUrlExist.get().getShortUrl());
    }

    private String getFormattedShortUrl(String shortUrl) {
        return Formatter.formatShortUrl(shortUrl);
    }

    @Override
    public List<UrlDto> getTheMainPage(Long userId) {
        var urlsList = urlRepository.findByUserId(userId);
        return urlsList.isPresent() ? changeShortUrl(urlsList) : Collections.emptyList();
    }

    private List<UrlDto> changeShortUrl(Optional<List<Url>> urlsList) {
        var mainPageTable = urlMapper.entityToUrlDto(urlsList.get());
        mainPageTable.forEach(main -> main.setShortUrl(getFormattedShortUrl(main.getShortUrl())));
        return mainPageTable;
    }

    private void saveUrl(Long userId, String fullUrl, String shortUrl) {
        Url urlBuild = Url.builder()
                .creationDate(LocalDate.now())
                .fullUrl(fullUrl)
                .shortUrl(shortUrl)
                .visitCount(0)
                .userId(userId)
                .build();
        urlRepository.save(urlBuild);
    }

    private String randomShortUrlAndCheckDuplicate() {
        var randUrl = RandomStringUtils.random(9, true, true);
        while (urlRepository.findByShortUrl(randUrl).isPresent()) {
            randUrl = RandomStringUtils.random(9, true, true);
        }
        return randUrl;
    }

}
