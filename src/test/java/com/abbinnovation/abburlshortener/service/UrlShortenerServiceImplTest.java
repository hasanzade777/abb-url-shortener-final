package com.abbinnovation.abburlshortener.service;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.abbinnovation.abburlshortener.dto.url.UrlDto;
import com.abbinnovation.abburlshortener.mapper.UrlMapper;
import com.abbinnovation.abburlshortener.model.Url;
import com.abbinnovation.abburlshortener.model.UrlVisitCountLog;
import com.abbinnovation.abburlshortener.repository.UrlRepository;
import com.abbinnovation.abburlshortener.service.impl.UrlShortenerServiceImpl;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UrlShortenerServiceImplTest {
    @Mock
    private UrlRepository urlRepository;
    @Mock
    private UrlMapper urlMapper;
    @InjectMocks
    private UrlShortenerServiceImpl urlShortenerService;

    private Url url;
    private UrlDto urlDto;

    @BeforeEach
    void setup() {
        UrlVisitCountLog urlVisitCountLog = UrlVisitCountLog.builder()
                .id(1L)
                .urlId(url)
                .visitDate(LocalDate.of(2023, 1, 1))
                .visitCount(5)
                .build();
        url = Url.builder()
                .fullUrl("facebook.com")
                .shortUrl("2kDlMvbsQ")
                .creationDate(LocalDate.of(2023, 1, 1))
                .visitCount(100)
                .userId(1L)
                .urlVisitCountLogs(Set.of(urlVisitCountLog))
                .build();
        urlDto = UrlDto.builder()
                .fullUrl("facebook.com")
                .shortUrl("https://shorten-url.me/2kDlMvbsQ")
                .creationDate(LocalDate.of(2023, 1, 1))
                .visitCount(100)
                .build();
    }

    @Test
    void givenFullUrlExistToShortUrlTest() {
        //Arrange
        when(urlRepository.findByFullUrlAndUserId("facebook.com", 1L)).thenReturn(Optional.of(url));

        //Act
        var url = urlShortenerService.shortenUrl(1L, "facebook.com");

        //Assert
        verify(urlRepository).findByFullUrlAndUserId("facebook.com", 1L);
        assertThat(url).isNotBlank();
        assertThat(url).isEqualTo("https://shorten-url.me/2kDlMvbsQ");

    }

    @Test
    void givenFullUrlIsNullOrEmptyToShortUrlTest() {
        //Arrange

        //Act
        var url = urlShortenerService.shortenUrl(1L, "");

        //Assert
        assertThat(url).isEqualTo("URL can't be empty or blank this is message for ATTENTION");

    }

    @Test
    void givenFullUrlNotExistToShortUrlTest() {
        //Arrange
        when(urlRepository.findByFullUrlAndUserId("facebook.com", 1L)).thenReturn(Optional.empty());

        //Act
        var url = urlShortenerService.shortenUrl(1L, "facebook.com");

        //Assert
        verify(urlRepository).findByFullUrlAndUserId("facebook.com", 1L);
        assertThat(url).isNotBlank();
        assertThat(url).hasSize(32);
    }

    @Test
    void getTheMainPageTest() {
        //Arrange
        when(urlRepository.findByUserId(1L)).thenReturn(Optional.of(singletonList(url)));
        when(urlMapper.entityToUrlDto(singletonList(url))).thenReturn(singletonList(urlDto));

        //Act
        var listUrlDto = urlShortenerService.getTheMainPage(1L);

        verify(urlRepository).findByUserId(1L);
        assertThat(listUrlDto).isNotNull();
        assertThat(listUrlDto).isEqualTo(singletonList(urlDto));
    }
}
