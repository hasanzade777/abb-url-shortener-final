package com.abbinnovation.abburlshortener.controller;


import com.abbinnovation.abburlshortener.model.Url;
import com.abbinnovation.abburlshortener.service.UrlVisitCountLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Optional;

import static com.abbinnovation.abburlshortener.consts.StringConsts.HTTPS;

@RequiredArgsConstructor
@Controller
@Slf4j
@RequestMapping("/")
public class UrlRedirectController {
    private final UrlVisitCountLogService visitCountLogService;

    @GetMapping("/{shortUrl}")
    public RedirectView redirectToFullUrl(@PathVariable("shortUrl") String shortUrl) {
        Optional<Url> optionalUrl = visitCountLogService.getUrlByShortUrl(shortUrl);
        if (optionalUrl.isPresent()) {
            Url url = optionalUrl.get();
            visitCountLogService.updateVisitCount(url);
            if (!url.getFullUrl().startsWith(HTTPS)) {
                return new RedirectView(HTTPS + url.getFullUrl());
            } else {
                return new RedirectView(url.getFullUrl());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Short URL not found.");
        }
    }
}
