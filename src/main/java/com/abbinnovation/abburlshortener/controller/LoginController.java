package com.abbinnovation.abburlshortener.controller;

import com.abbinnovation.abburlshortener.dto.login.LoginInfo;
import com.abbinnovation.abburlshortener.utils.CookieUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequiredArgsConstructor
public class LoginController {
    @Value("${login.rememberme.time.seconds}")
    private Integer rememberMeTimeSeconds;

    @GetMapping("/")
    public String main() {
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("user", new LoginInfo());
        return "login";
    }

    @PostMapping("/login")
    public String check(@ModelAttribute("user") LoginInfo loginInfo,
                        HttpServletRequest req, HttpServletResponse resp) {
        if (loginInfo.getIsRemembered()) {
            CookieUtil.extendSessionExpiration(req, resp, rememberMeTimeSeconds);
        }
        return "redirect:/url/landing";
    }
}
