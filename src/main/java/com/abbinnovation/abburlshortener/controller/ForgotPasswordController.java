package com.abbinnovation.abburlshortener.controller;

import com.abbinnovation.abburlshortener.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
@RequestMapping("/forgot-password")
public class ForgotPasswordController {
    private final UserService userService;

    @GetMapping
    public String showForgotPasswordPage(Model model) {
        model.addAttribute("forgot-password", "Forgot password");
        return "forgot-password";
    }

    @PostMapping
    public String forgotPasswordWithEmailSending(@RequestParam String email, Model model) {
        model.addAttribute("email", email);
        userService.forgotPassword(email);
        return "redirect:/login";
    }
}
